package com.rentmoola.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.Button;

import com.rentmoola.app.https.ApiActivity;

public class DocVerifyThanksActivity extends ApiActivity
{

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_doc_verify_thanks);
        // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // setSupportActionBar(toolbar);

        Activity activity = this;

        m_LoginPrefs = getSharedPreferences(m_LoginPrefsFile, Context.MODE_PRIVATE);
        m_LoginPrefs.edit().putBoolean("identity_checked", true).commit();

        Button ok_button = (Button) findViewById(R.id.thanks_ok_button);
        ok_button.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Intent intent = new Intent(activity, WebViewActivity.class);
                intent.putExtra("url",  "login");
                startActivity(intent);
            }
        });
    }

}
