package com.rentmoola.app;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class IdentityCheckData extends JSONObject
{
    private JSONArray m_DataSourceResults = new JSONArray();

    public RMDataSourceResult CreateDataSourceResult(String name)
    {
        RMDataSourceResult data_source_result = new RMDataSourceResult(name);
        m_DataSourceResults.put(data_source_result);

        return data_source_result;
    }

    public ArrayList<String> GetStringArrayList()
    {
        ArrayList<String> string_array = new ArrayList<String>();
        int arrayIndex;
        JSONObject jsonArrayItem;
        String jsonArrayItemKey;

        for(int i=0; i <  m_DataSourceResults.length(); ++i)
        {
            try
            {
                JSONObject item = m_DataSourceResults.getJSONObject(i);
                String json = item.toString();

                string_array.add(json);
            }
            catch (JSONException e)
            {
                e.printStackTrace();
            }
        }

        return string_array;
    }
}
