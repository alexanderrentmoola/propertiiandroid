package com.rentmoola.app;

import android.content.Context;
import android.graphics.Bitmap;
import android.graphics.BitmapFactory;
import android.graphics.Rect;
import android.net.Uri;
import android.support.v4.content.AsyncTaskLoader;

import java.io.FileNotFoundException;
import java.io.InputStream;

/**
 * Loader that loads an image and crops it to the given bounds
 */
class ImageLoader extends AsyncTaskLoader<Bitmap> {

    private Uri imageUri;
    private Rect faceBounds;

    public ImageLoader(Context context, Uri imageUri, Rect faceBounds) {
        super(context);
        this.imageUri = imageUri;
        this.faceBounds = faceBounds;
    }

    @Override
    public Bitmap loadInBackground() {
        try {
            InputStream inputStream = getContext().getContentResolver().openInputStream(imageUri);
            Bitmap bitmap = BitmapFactory.decodeStream(inputStream);
            if (bitmap != null) {
                Rect imageRect = new Rect(0, 0, bitmap.getWidth(), bitmap.getHeight());
                // Add a bit of space around the face for better detection
                faceBounds.inset(0-(int)((double)faceBounds.width()*0.1), 0-(int)((double)faceBounds.height()*0.1));
                // Ensure the face is contained within the bounds of the image
                //noinspection CheckResult
                imageRect.intersect(faceBounds);
                return Bitmap.createBitmap(bitmap, imageRect.left, imageRect.top, imageRect.width(), imageRect.height());
            }
        } catch (FileNotFoundException e) {
            e.printStackTrace();
        }
        return null;
    }
}