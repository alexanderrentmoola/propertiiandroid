package com.rentmoola.app;

import org.json.JSONException;

public class DataSourceField extends FieldData
{
    public DataSourceField(String name, String status)
    {
        super(name);
        SetStatus(status);
    }

    public void SetStatus(String status)
    {
        try
        {
            put("status", status);
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public String GetStatus()
    {
        String status = "";

        try
        {
            status = getString("status");
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
        return status;
    }
}