package com.rentmoola.app;

import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Map;
import java.util.ArrayList;

import com.appliedrec.ver_ididcapture.data.IDDocument;
import com.rentmoola.app.https.ApiThreadHandler;
import com.rentmoola.app.https.RMGetRequest;
import com.rentmoola.core.model.user.RmLandlord;
import com.trulioo.normalizedapi.*;
import com.trulioo.normalizedapi.api.*;
import com.trulioo.normalizedapi.model.*;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;


public class IdentityCheckerTrulioo extends DocumentCheckerBase
{
    private ApiClient m_ApiClient;

    protected IDDocument m_FaceRecIdDocument;

    private ConnectionApi m_ConnectionApi;
    private ConfigurationApi m_ConfigurationApi;

    private List<String> m_CountryCodes;
    private Map<String, List<String>> m_CADocumentTypes;
    private Map<String, List<String>> m_USDocumentTypes;

    private List<DataFields> m_TestEntities;
    private VerificationsApi m_VerificationClient;
    private RMGetRequest m_RMGetRequest;

    private RmLandlord m_Landlord;
    private IdentityCheckData m_IdentityCheckData;
    private LandlordCheckActivity m_Activity;

    public IdentityCheckerTrulioo(RmLandlord landlord, IDDocument document, LandlordCheckActivity activity, String username, String password)
    {
        super(activity, username, password);

        m_Landlord = landlord;
        m_FaceRecIdDocument = document;
        m_Activity = activity;
    }

    final int c_ExpiryDate = 1;
    final int c_Birthdate = 2;
    final int c_IIN = 3;
    final int c_DLID = 4;
    final int c_Address1 = 7;
    final int c_Address2 = 8;
    final int c_FirstName = 9;
    final int c_City = 10;
    final int c_LastName = 11;
    final int c_StateProvince = 12;
    final int c_EyeColor = 15;
    final int c_Security = 16;
    final int c_Sex = 17;
    final int c_Version = 18;
    final int c_Height = 19;
    final int c_HairColor = 20;
    final int c_PostalCode = 21;
    final int c_Weight = 22;

    // Extract data from String array where Strings are in the
    // form "key: value" (including space)
    private String GetValue(int data_index, final String desc_strings[])
    {
        final String name_value_pair = desc_strings[data_index];
        final String strings[] = name_value_pair.split(": ");

        return SanitizeString(strings[1]);
    }

    // Remove all the scrunty stuff we don't want8
    private String SanitizeString(final String string)
    {
        String temp = string;
        temp = temp.replace(",", "");

        return temp;
    }

    public void Verify()
    {
        final String desc = m_FaceRecIdDocument.getDescription();
        final String desc_strings[] = desc.split("\n");

        final String address1 = GetValue(c_Address1, desc_strings);
        final String first_name = GetValue(c_FirstName, desc_strings);
        final String last_name = GetValue(c_LastName, desc_strings);
        final String birth_date = GetValue(c_Birthdate, desc_strings);
        final String postal_code = GetValue(c_PostalCode, desc_strings);
        final String state_province = GetValue(c_StateProvince, desc_strings);
        final String city = GetValue(c_City, desc_strings);
        final String sex = GetValue(c_Sex, desc_strings);
        final String dl_number = GetValue(c_DLID, desc_strings);

        final String split_birth_date[] = birth_date.split("/");
        final Integer birth_date_day = Integer.parseInt(split_birth_date[0]);
        final Integer birth_date_month = Integer.parseInt(split_birth_date[1]);
        final Integer birth_date_year = Integer.parseInt(split_birth_date[2]);

        final String split_address1[] = address1.split(" ");
        final String building_number = split_address1[0];
        final String street_name = split_address1[1];
        final String street_name_type = split_address1[2];

        ApiThreadHandler m_ApiThreadHandler;

        String street_type = "Street";
        if(street_name_type == "RD")
        {
            street_type = "Road";
        }
        else
        if(street_name_type == "DR")
        {
            street_type = "Drive";
        }
        else
        if(street_name_type == "Av")
        {
            street_type = "Ave";
        }

        m_VerificationClient = new VerificationsApi(m_ApiClient);

        /*
        VerifyRequest request = new VerifyRequest()
            .acceptTruliooTermsAndConditions(Boolean.TRUE)
            .demo(true)
            .configurationName("Identity Verification")
            .countryCode("CA")
            .consentForDataSources(new ArrayList<String>())
            .dataFields(new DataFields()
                    .personInfo(new PersonInfo()
                            .firstGivenName("JASON")
                            .firstSurName(last_name)
                            .yearOfBirth(birth_date_year)
                            .monthOfBirth(birth_date_month)
                            .dayOfBirth(birth_date_day))
                    .location(new Location()
                            .postalCode(postal_code)
                            .buildingNumber(building_number)
                            .streetName(street_name)
                            .streetType(street_type)
                            .stateProvinceCode(state_province)
                            .city(city))
                    .nationalIds(Arrays.asList(new NationalId().number(dl_number).type("DriversLicense")))
                    .document(new Document()
                            .documentBackImage("test image back".getBytes())
                            .documentFrontImage("test image front".getBytes())
                            .documentType("DrivingLicence"))
            );
            */

        m_RequestString = "Identity Verify";

        DataFields data0 = m_TestEntities.get(0);
        Location location = data0.getLocation();
        PersonInfo person_info = data0.getPersonInfo();
        List<NationalId> national_ids = data0.getNationalIds();

        VerifyRequest request = new VerifyRequest()
                .acceptTruliooTermsAndConditions(Boolean.TRUE)
                .demo(false)
                .configurationName("Identity Verification")
                .countryCode("CA")
                .consentForDataSources(new ArrayList<String>())
                .dataFields(new DataFields()
                        .personInfo(person_info)
                        .location(location)
                        .nationalIds(national_ids));

        try {
            m_VerificationClient.verifyAsync(request, new ApiCallback<VerifyResult>()
            {
                @Override
                public void onFailure(ApiException ae, int i, Map<String, List<String>> map)
                {
                    m_LastResponseCode = 400;
                }

                @Override
                public void onSuccess(VerifyResult t, int i, Map<String, List<String>> map)
                {
                    System.out.println(t);

                    m_IdentityCheckData = new IdentityCheckData();

                    Record record = t.getRecord();
                    GetData(record);

                    ArrayList<String> data_source_json_strings = m_IdentityCheckData.GetStringArrayList();
                    List<DatasourceResult> data_source_results = record.getDatasourceResults();
                    for(int j=0; j < data_source_json_strings.size(); ++j)
                    {
                        DatasourceResult data_source_result = data_source_results.get(j);
                        final String name = data_source_result.getDatasourceName();
                        final String data_source_result_json = data_source_json_strings.get(j);

                        // m_Landlord.addCustomField(name, data_source_result_json);

                       final String safe_name = name.replace(" ", "_");
                        String index_name = new Integer(j).toString();
                        m_Activity.AddToLandlordJSON(safe_name, data_source_result_json);
                    }

                    final String landlord_json = m_Landlord.toJson();
                    m_ResponseString = landlord_json;
                    m_RequestString = "landlord_verify";

                    SendResponse();
                }

                @Override
                public void onUploadProgress(long l, long l1, boolean bln) {
                    //To change body of generated methods, choose Tools | Templates.
                }

                @Override
                public void onDownloadProgress(long l, long l1, boolean bln) {
                    //To change body of generated methods, choose Tools | Templates.
                }
            });
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    void GetData(final Record record)
    {
        List<DatasourceResult> data_source_results = record.getDatasourceResults();
        for(int d=0; d < data_source_results.size(); ++d)
        {
            DatasourceResult data_source_result = data_source_results.get(d);

            final String status = data_source_result.getDatasourceStatus(); final String name = data_source_result.getDatasourceName();
            final List<DatasourceField> data_source_fields = data_source_result.getDatasourceFields();
            final List<AppendedField> appended_fields = data_source_result.getAppendedFields();

            String data_source_result_name = new Integer(d).toString();
            final String safe_name = name.replace(" ", "_");
            RMDataSourceResult rm_data_source_result = m_IdentityCheckData.CreateDataSourceResult(safe_name);

            for(int r2=0; r2 < appended_fields.size(); ++r2)
            {
                final AppendedField appended_field = appended_fields.get(r2);
                final String appended_field_name = appended_field.getFieldName();
                final String appended_field_data = appended_field.getData();

                rm_data_source_result.AddAppendedField(appended_field_name.replace(" ", "_"), appended_field_data);
            }

            for(int r1=0; r1 < data_source_fields.size(); ++r1)
            {
                final DatasourceField field = data_source_fields.get(r1);
                final String field_name = field.getFieldName();
                final String field_status = field.getStatus();

                rm_data_source_result.AddDataSourceField(field_name.replace(" ", "_"), field_status);
            }
        }
    }

    void getTestEntities()
    {
        if(m_ConfigurationApi == null)
        {
            m_ConfigurationApi = new ConfigurationApi(m_ApiClient);
        }

        try {
            m_TestEntities = m_ConfigurationApi.getTestEntities("Identity Verification", "CA");
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void Login()
    {
        m_ApiClient = new ApiClient();
        m_ApiClient.setUsername(m_Username);
        m_ApiClient.setPassword(m_Password);
        m_ConnectionApi = new ConnectionApi(m_ApiClient);

        try {
            m_ConnectionApi.testAuthenticationAsync(new ApiCallback<String>()
            {
                @Override
                public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
                    m_LastResponseCode = statusCode;
                }
                @Override
                public void onSuccess(String result, int statusCode, Map<String, List<String>> responseHeaders) {
                    m_LastResponseCode = statusCode;
                    m_ResponseString = result;

                    if(!m_Connected)
                    {
                        getTestEntities();

                        m_Connected = true;
                        GetCountryCodes();
                    }
                }
                @Override
                public void onUploadProgress(long bytesWritten, long contentLength, boolean done) {
                }
                @Override
                public void onDownloadProgress(long bytesRead, long contentLength, boolean done) {
                }
            });
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    private void GetCountryCodes()
    {
        m_ConfigurationApi = new ConfigurationApi(m_ApiClient);

        try {
            m_ConfigurationApi.getCountryCodesAsync("Identity Verification", new ApiCallback<List<String>>()
            {
                @Override
                public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
                    Log.e("GET RESPONSE", "FAILED!");
                }
                @Override
                public void onSuccess(List<String> result, int statusCode, Map<String, List<String>> responseHeaders) {
                    System.out.println(result); //To change body of generated methods, choose Tools | Templates.
                    m_CountryCodes = result;

                    m_CADocumentTypes = GetDocuments(m_CountryCodes.get(0));
                    m_USDocumentTypes = GetDocuments(m_CountryCodes.get(1));
                }
                @Override
                public void onUploadProgress(long bytesWritten, long contentLength, boolean done) {
                }
                @Override
                public void onDownloadProgress(long bytesRead, long contentLength, boolean done) {
                }
            });
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    private Map<String, List<String>> GetDocuments(final String country_code)
    {
        Map<String, List<String>> documents = null;
        try
        {
            m_ConfigurationApi.getDocumentTypesAsync("AU", new ApiCallback<Map<String, List<String>>>()
            {
                @Override
                public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
                    Log.e("GET RESPONSE", "FAILED!");
                }
                @Override
                public void onSuccess(Map<String, List<String>> result, int statusCode, Map<String, List<String>> responseHeaders)
                {
                    System.out.println(result);

                    if(country_code.equals("CA"))
                    {
                        m_CADocumentTypes = result;
                        m_RequestString = "CADocuments";
                    }
                    else
                    if(country_code.equals("US"))
                    {
                        m_USDocumentTypes = result;
                        m_RequestString = "USDocuments";
                    }

                    m_ResponseString = "Trulioo is ready!";
                    SendResponse();
                }
                @Override
                public void onUploadProgress(long bytesWritten, long contentLength, boolean done) {
                }
                @Override
                public void onDownloadProgress(long bytesRead, long contentLength, boolean done) {
                }
            });
        } catch (ApiException e) {
            e.printStackTrace();
        }
        return documents;
    }

    public void WikiTest()
    {
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));

        HttpParams params = new BasicHttpParams();

        SingleClientConnManager mgr = new SingleClientConnManager(params, schemeRegistry);

        HttpClient client = new DefaultHttpClient(mgr, params);
        String getURL = "https://wikipedia.org";
        HttpGet get = new HttpGet(getURL);
        HttpResponse responseGet = null;
        try {
            responseGet = client.execute(get);
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpEntity resEntityGet = responseGet.getEntity();
        if (resEntityGet != null) {
            //do something with the response
            try {

                String data = EntityUtils.toString(resEntityGet);
                Log.e("GET RESPONSE", data);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
