package com.rentmoola.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.graphics.Rect;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.LoaderManager;
import android.support.v4.content.AsyncTaskLoader;
import android.support.v4.content.Loader;
import android.support.v4.graphics.drawable.RoundedBitmapDrawable;
import android.support.v4.graphics.drawable.RoundedBitmapDrawableFactory;
import android.util.Base64;
import android.view.View;

import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.appliedrec.ver_id.VerID;
import com.appliedrec.ver_id.VerIDLivenessDetectionIntent;
import com.appliedrec.ver_id.model.VerIDFace;
import com.appliedrec.ver_id.session.VerIDLivenessDetectionSessionSettings;
import com.appliedrec.ver_id.session.VerIDSessionResult;
import com.appliedrec.ver_id.ui.VerIDActivity;
import com.appliedrec.ver_id.util.FaceUtil;
import com.appliedrec.ver_ididcapture.data.IDDocument;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rentmoola.app.https.ApiActivity;
import com.rentmoola.app.https.ApiResponse;
import com.rentmoola.app.https.ApiThreadHandler;
import com.rentmoola.app.https.RMGetApiRequest;
import com.rentmoola.app.https.RMGetRequest;
import com.rentmoola.app.https.RMPostApiRequest;
import com.rentmoola.app.https.RMPostRequest;
import com.rentmoola.core.model.RmModel;
import com.rentmoola.core.model.user.RmLandlord;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.nio.ByteBuffer;

import static com.rentmoola.core.model.user.RmLandlord.APPLICATION_STATUS.IDENTITY_CONFIRMED;

public class IdScanResultActivity extends FaceRecActivity implements LoaderManager.LoaderCallbacks
{
    private static final int LOADER_ID_CARD_FACE = 86;

    // Extra name constant
    public static final String EXTRA_LIVENESS_DETECTION_RESULT = "com.appliedrec.ver_ididcapture.EXTRA_LIVENESS_DETECTION_RESULT";

    // Loader IDs
    private static final int LOADER_ID_SCORE = 458;
    private static final int LOADER_ID_LIVE_FACE = 355;

    // Views
    private ImageView liveFaceView;
    private ImageView cardFaceView;


    // Extracted card and face images
    private Bitmap cardFaceImage;

    IDDocument idDocument;
    VerIDSessionResult verIDSessionResult;
    private ImageView cardImageView;
    private View scrollView;

    Button b1,b2, m_OkButton;
    TextView m_FirstNameEdit;
    TextView m_LastNameEdit;
    EditText m_Address1Edit;
    EditText m_Address2Edit;
    EditText m_CityEdit;
    EditText m_PostalCodeEdit;

    // TextView m_ScoreView;

    TextView tx1;
    int counter = 3;
    ApiActivity m_Activity;

    private String m_CardFaceImageEncoded = new String();
    private String m_LiveFaceImageEncoded = new String();

    private RMGetApiRequest m_RMGetRequest;
    protected RMPostApiRequest m_RMPostApiRequest;
    private String m_Url;

    static private boolean m_ConfirmingIdentity;

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.id_scan_result);

        m_ConfirmingIdentity = false;
        m_Activity = this;

        cardFaceView = findViewById(R.id.card_face);
        liveFaceView = findViewById(R.id.live_face);

        // m_ScoreView = findViewById(R.id.score_view);

        m_FirstNameEdit = findViewById(R.id.FirstNameEdit);
        m_LastNameEdit = findViewById(R.id.LastNameEdit);
        m_Address1Edit = findViewById(R.id.Address1Edit);
        m_Address2Edit = findViewById(R.id.Address2Edit);
        m_CityEdit = findViewById(R.id.CityEdit);
        m_PostalCodeEdit = findViewById(R.id.PostalCodeEdit);

        idDocument = CardCaptureResultPersistence.loadCapturedDocument(IdScanResultActivity.this);

        Intent intent = getIntent();
        getSupportLoaderManager().initLoader(LOADER_ID_CARD_FACE, intent.getExtras(), this).forceLoad();

        liveFaceCompareButton = (Button) findViewById(R.id.authenticate);
        liveFaceCompareButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                // Collect a live selfie and compare it to the face on the ID card
                VerIDLivenessDetectionSessionSettings sessionSettings = new VerIDLivenessDetectionSessionSettings();
                sessionSettings.expiryTime = 120000;
                // Ask Ver-ID to extract face templates needed for face recognition
                sessionSettings.includeFaceTemplatesInResult = true;
                Intent intent = new VerIDLivenessDetectionIntent(IdScanResultActivity.this, sessionSettings);
                startActivityForResult(intent, REQUEST_CODE_FACE);
            }
        });

        // Load Ver-ID
        VerID.shared.load(this, new VerID.LoadCallback() {
            @Override
            public void onLoad()
            {
                if (!isDestroyed())
                {
                    AsyncTask.execute(new Runnable() {
                        @Override
                        public void run() {

                            // Load captured ID card result from shared preferences
                            idDocument = CardCaptureResultPersistence.loadCapturedDocument(IdScanResultActivity.this);
                            // Check that the card has a face that's suitable for recognition
                            if (idDocument != null && idDocument.getFaceSuitableForRecognition() == null) {
                                //^^ idDocument = null;
                                // Delete the card, it cannot be used for face recognition
                                //^^ CardCaptureResultPersistence.saveCapturedDocument(MainActivity.this, null);
                            }
                            runOnUiThread(new Runnable() {
                                @Override
                                public void run() {
                                    if (!isDestroyed()) {
                                        setShowProgressBar(false);
                                        UpdateContent();
                                    }
                                }
                            });
                        }
                    });
                }
            }

            @Override
            public void onError(Exception error) {
                // loadingIndicatorView.setVisibility(View.GONE);
                Toast.makeText(IdScanResultActivity.this, R.string.verid_failed_to_load, Toast.LENGTH_SHORT).show();
            }
        });

        DoLoginRequest();
    }

    @Override
    public void handleRequestResponse(ApiResponse api_response)
    {
        final String response = api_response.GetResponseString();
        final int response_code = api_response.GetResponseCode();
        if(response_code == 200)
        {
            if(m_Url.contains("/authorizeapi") || m_Url.contains("/confirmidentity"))
            {
                HandleApiSigninResponse(response);
            }
        }
    }

    protected void StoreFaceIdComparisonResult(Float score)
    {
        final String score_string = score.toString();

        // m_ScoreView.setText("Score: " + score_string);

        m_Landlord = LoadLandlord();
        if(m_Landlord != null)
        {
            m_Activity.AddToLandlordJSON("face_rec_score", score_string);

            // if(score > 0.5)
            {
               // m_Landlord.setApplicationStatus(IDENTITY_CONFIRMED);

                // Save face_rec_score to custom fields
                // SaveLandlord();

                ConfirmLandlordIdentity();

                /*
                final String url = ApiActivity.m_ApiDomain + "update";
                ApiThreadHandler handler = new ApiThreadHandler(this);

                JSONObject email_data = new JSONObject();
                try {
                    email_data.put("to", m_Landlord.getEmail());
                    email_data.put("subject", "You Identity Has Been Verified!");
                    email_data.put("text", "Please log in");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String email_json = email_data.toString();
                m_RMPostRequest = new RMPostRequest("https://192.168.0.198:5000/sendpost", this, handler, email_json);
                m_RMPostRequest.Start();
                */
            }

            // if(score > 0.5)
            {


                // Intent intent = new Intent(this, LandlordCheckActivity.class);
                // startActivity(intent);
            }
        }
    }

    private String RemoveDescPrefix(String prefix_name, String desc)
    {
        return desc.replaceAll(prefix_name + ":", "");
    }

    private void UpdateContent()
    {
        final String desc = idDocument.getDescription();
        final String desc_strings[] = desc.split("\n");

        final int c_ExpiryDate = 1;
        final int c_Birthdate = 2;
        final int c_IIN = 3;
        final int c_DLID = 4;
        final int c_Address1 = 7;
        final int c_Address2 = 8;
        final int c_FirstName = 9;
        final int c_City = 10;
        final int c_LastName = 11;
        final int c_StateProvince = 12;
        final int c_EyeColor = 15;
        final int c_Security = 16;
        final int c_Sex = 17;
        final int c_Version = 18;
        final int c_Height = 19;
        final int c_HairColor = 20;
        final int c_PostalCode = 21;
        final int c_Weight = 22;

        desc_strings[c_LastName] = desc_strings[c_LastName].replaceAll(",", "");

        m_FirstNameEdit.setText(RemoveDescPrefix("First name", desc_strings[c_FirstName]));
        m_LastNameEdit.setText(RemoveDescPrefix("Last name", desc_strings[c_LastName]));
        m_Address1Edit.setText(RemoveDescPrefix("Address", desc_strings[c_Address1]));
        m_Address2Edit.setText(desc_strings[c_Address2]);
        m_CityEdit.setText(RemoveDescPrefix("City", desc_strings[c_City]));
        m_PostalCodeEdit.setText(RemoveDescPrefix("Postal code", desc_strings[c_PostalCode]));

    }

    private void setShowProgressBar(boolean show)
    {
        /*
        loadingIndicatorView.setVisibility(show ? View.VISIBLE : View.GONE);
        scanIdButton.setVisibility(show ? View.GONE : View.VISIBLE);
        liveFaceCompareButton.setVisibility(show ? View.GONE : View.VISIBLE);
        */
    }

    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        if(requestCode == REQUEST_CODE_FACE && resultCode == RESULT_OK && data != null)
        {
            // Received a selfie response
            verIDSessionResult = data.getParcelableExtra(VerIDActivity.EXTRA_SESSION_RESULT);
            if (verIDSessionResult != null) {
                if (verIDSessionResult.isPositive()) {
                    // Compare the selfie with the face on the ID card
                    compareLiveFace();
                    return;
                } else if (verIDSessionResult.outcome == VerIDSessionResult.Outcome.FAIL_NUMBER_OF_RESULTS) {
                    // No faces detected
                    Toast.makeText(this, R.string.failed_to_detect_face, Toast.LENGTH_SHORT).show();
                    return;
                }
            }
            // Selfie capture failed
            Toast.makeText(this, R.string.live_face_capture_failed, Toast.LENGTH_SHORT).show();
        }
    }

    private void compareLiveFace()
    {
        // Ensure we have a valid ID capture result to compare the selfie to
        if (verIDSessionResult != null && verIDSessionResult.isPositive() &&
            !verIDSessionResult.getFaceImages(VerID.Bearing.STRAIGHT).isEmpty() && idDocument != null && idDocument.getFaceSuitableForRecognition() != null)
        {
            idDocument = CardCaptureResultPersistence.loadCapturedDocument(this);
            Intent intent = getIntent();
            if (idDocument != null && idDocument.getFaceSuitableForRecognition() != null && intent != null)
            {
                if (verIDSessionResult != null && verIDSessionResult.getFacesSuitableForRecognition(VerID.Bearing.STRAIGHT).length > 0)
                {
                    // Get the cropped face images
                    getSupportLoaderManager().initLoader(LOADER_ID_CARD_FACE, intent.getExtras(), this).forceLoad();
                    getSupportLoaderManager().initLoader(LOADER_ID_LIVE_FACE, intent.getExtras(), this).forceLoad();
                    getSupportLoaderManager().initLoader(LOADER_ID_SCORE, null, this).forceLoad();
                }
            }
        }
        else
        {
            Intent intent = new Intent(this, DocVerifyThanksActivity.class);
            startActivity(intent);
        }
    }

    @Override
    public Loader onCreateLoader(int id, Bundle args)
    {
        switch (id) {
            case LOADER_ID_CARD_FACE:
                Uri cardImageUri = idDocument.getImageUri();
                Rect cardImageFaceBounds = new Rect();
                idDocument.getFaceSuitableForRecognition().getBounds().round(cardImageFaceBounds);
                return new ImageLoader(this, cardImageUri, cardImageFaceBounds);
            case LOADER_ID_LIVE_FACE:
                VerIDFace face = verIDSessionResult.getFacesSuitableForRecognition(VerID.Bearing.STRAIGHT)[0];
                Uri faceImageUri = verIDSessionResult.getFaceImages().get(face);
                Rect faceImageFaceBounds = new Rect();
                face.getBounds().round(faceImageFaceBounds);
                return new ImageLoader(this, faceImageUri, faceImageFaceBounds);
            case LOADER_ID_SCORE:
                return new ScoreLoader(this, idDocument.getFace(), verIDSessionResult.getFacesSuitableForRecognition(VerID.Bearing.STRAIGHT));
            default:
                return null;

        }
    }

    private static class ScoreLoader extends AsyncTaskLoader<Float>
    {
        private VerIDFace cardFace;
        private VerIDFace[] liveFaces;

        public ScoreLoader(Context context, VerIDFace cardFace, VerIDFace[] liveFaces) {
            super(context);
            this.cardFace = cardFace;
            this.liveFaces = liveFaces;
        }

        @Override
        public Float loadInBackground() {
            Float score = null;
            for (VerIDFace face : liveFaces) {
                try {
                    float faceScore = FaceUtil.compareFaces(cardFace, face);
                    if (score == null) {
                        score = faceScore;
                    } else {
                        score = Math.max(faceScore, score);
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            return score;
        }
    }

    public static String BitmapToBase64(Bitmap image)
    {
        Bitmap immagex = image;
        ByteArrayOutputStream baos = new ByteArrayOutputStream();
        immagex.compress(Bitmap.CompressFormat.PNG, 90, baos);
        byte[] b = baos.toByteArray();
        String imageEncoded = Base64.encodeToString(b, Base64.DEFAULT);
        return imageEncoded;
    }

    public void StoreBitmap(String name, Bitmap image)
    {
        // final Bitmap scaled_bmp = Bitmap.createScaledBitmap(image, 256, 256, true);


    }

    @Override
    public void onLoadFinished(Loader loader, Object data)
    {
        switch (loader.getId()) {
            case LOADER_ID_CARD_FACE:
                if (data != null) {
                    cardFaceImage = (Bitmap) data;
                    if (Build.VERSION.SDK_INT >= 21) {
                        RoundedBitmapDrawable drawable = RoundedBitmapDrawableFactory.create(getResources(), cardFaceImage);
                        drawable.setCornerRadius((float) drawable.getIntrinsicWidth() / 10f);
                        cardFaceView.setImageDrawable(drawable);
                    } else {
                        cardFaceView.setImageBitmap(cardFaceImage);
                    }

                    if(m_CardFaceImageEncoded.length() == 0) {
                        m_CardFaceImageEncoded = BitmapToBase64(cardFaceImage);
                        AddToLandlordJSON("card_face_image", m_CardFaceImageEncoded);
                    }
                }
                break;
            case LOADER_ID_LIVE_FACE:
                if (data != null) {
                    liveFaceImage = (Bitmap) data;

                        liveFaceView.setImageBitmap(liveFaceImage);

                    if(m_LiveFaceImageEncoded.length() == 0) {
                        m_LiveFaceImageEncoded = BitmapToBase64(liveFaceImage);
                        AddToLandlordJSON("live_face_image", m_LiveFaceImageEncoded);
                    }
                }
                break;
            case LOADER_ID_SCORE:
                Float score = (Float) data;
                StoreFaceIdComparisonResult(score);
                break;
        }
    }


    @Override
    public void onLoaderReset(Loader loader) {
        switch (loader.getId()) {
            case LOADER_ID_CARD_FACE:
                cardFaceView.setImageDrawable(null);
                break;
            case LOADER_ID_LIVE_FACE:
                liveFaceView.setImageDrawable(null);
                break;
            case LOADER_ID_SCORE:
                break;
        }
    }

    // android:networkSecurityConfig="@xml/network_security_config"

    protected void ConfirmLandlordIdentity()
    {
        if(!m_ConfirmingIdentity)
        {
            m_Url = ApiActivity.m_ApiDomain + "landlord/" + m_Landlord.getId().toString() + "/confirmidentity";
            m_ApiThreadHandler = new ApiThreadHandler(this);

            m_RMPostApiRequest = new RMPostApiRequest(m_Url, this, m_ApiThreadHandler, m_SessionRole.toJson());
            m_RMPostApiRequest.Start();

            m_ConfirmingIdentity = true;
        }
    }

    public void DoLoginRequest()
    {
        m_Url = ApiActivity.m_ApiDomain + "authorizeapi";
        m_ApiThreadHandler = new ApiThreadHandler(this);
        m_RMGetRequest = new RMGetApiRequest(m_Url, this, m_ApiThreadHandler, "androidapptemp", "89545242463746874");
        m_RMGetRequest.Start();
    }

    private void HandleApiSigninResponse(String response)
    {
        try
        {
            m_ResponseJson = new JSONObject(response);
        } catch (JSONException e)
        {
            e.printStackTrace();
        }

        try
        {
            if(m_Url.contains("authorizeapi"))
            {
                m_ApiToken = GetTokenFromResponse(m_ResponseJson.getString("access_token"));
                m_Url = "";
            }
            else
            if(m_Url.contains("confirmidentity"))
            {
                // JsonNode node = m_ResponseJson.get("sessionRole");
                // m_Landlord =  (RmLandlord)RmModel.fromJson(node.asText());

                // m_Landlord = (RmLandlord) RmLandlord.fromJson(m_ResponseJson.toString());

                m_Landlord.setApplicationStatus(IDENTITY_CONFIRMED);
                m_Landlord.setCustomFields(m_LandlordCustomFields);

                final String url = ApiActivity.m_ApiDomain + "update";
                ApiThreadHandler handler = new ApiThreadHandler(this);

                JSONObject email_data = new JSONObject();
                try {
                    email_data.put("to", m_Landlord.getEmail());
                    email_data.put("subject", "You Identity Has Been Verified!");
                    email_data.put("text", "Please log in");

                } catch (JSONException e) {
                    e.printStackTrace();
                }

                final String email_json = email_data.toString();
                // m_RMPostRequest = new RMPostRequest("https://192.168.0.198:5000/sendpost", this, handler, email_json);
                // m_RMPostRequest.Start();

                SaveLandlord();

                m_Url = "";

                Intent intent = new Intent(this, LandlordCheckActivity.class);
                startActivity(intent);
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }


    }

    protected String GetTokenFromResponse(String response_text)
    {
        String split_jwt_token[] = null;

        final String token  = response_text;
        split_jwt_token = token.split("\\.");

        String decoded_jwt_token = null;
        byte[] data = Base64.decode(split_jwt_token[1], Base64.DEFAULT);
        try
        {
            decoded_jwt_token = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
            return null;
        }

        JSONObject json = null;
        try
        {
            json = new JSONObject(decoded_jwt_token);
        } catch (JSONException e)
        {
            e.printStackTrace();
            return null;
        }

        JsonNode jsonObject = null;
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try
        {
            jsonObject = mapper.readTree(String.valueOf(json));
        } catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }

        JsonNode node = jsonObject.get("sessionRole");
        m_SessionRole = RmModel.fromJson(node.asText());
        return token;
    }

}