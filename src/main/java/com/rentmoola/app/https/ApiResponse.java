package com.rentmoola.app.https;

import org.json.JSONException;
import org.json.JSONObject;

public class ApiResponse {

    public JSONObject m_JSONData = null;
    public String m_RequestUrl;
    public int m_ResponseCode;
    public String m_ResponseString;

    public String GetRequestUrl()
    {
        return m_RequestUrl;
    }

    public ApiResponse(String request_url, String response_code, String response)
    {
        m_RequestUrl = request_url;
        if(response != null)
        {
            if(response_code != null)
            {
                m_ResponseCode = Integer.parseInt(response_code);
            }

            m_ResponseString = response;

            try {
                m_JSONData = new JSONObject(m_ResponseString);
            } catch (JSONException e) {
                e.printStackTrace();
            }
        }
    }

    public String GetErrorMessage()
    {
        String error_message = "";
        if(m_JSONData != null)
        {
            try
            {
                error_message = m_JSONData.getString("error_message");
            } catch (JSONException e)
            {
                e.printStackTrace();
            }
            return error_message;
        }
        return error_message;
    }

    public String GetValueFromJSON(String name)
    {
        String value = "";
        if(m_JSONData != null)
        {
            try
            {
                value = m_JSONData.getString(name);
            } catch (JSONException e)
            {
                e.printStackTrace();
            }
            return value;
        }
        return value;
    }

    public JSONObject getJSONData(){
        return m_JSONData;
    }
    public String GetResponseString(){ return m_ResponseString; }
    public String GetRequestURL(){ return m_RequestUrl; }
    public int GetResponseCode(){ return m_ResponseCode; }
}
