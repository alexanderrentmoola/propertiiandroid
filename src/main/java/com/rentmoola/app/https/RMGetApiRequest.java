package com.rentmoola.app.https;
import android.content.Context;
import android.os.Handler;

public class RMGetApiRequest extends RMApiClient
{
    public RMGetApiRequest(String url, Context context, Handler handler, String json_params)
    {
        super(RMApiClient.c_Get, url, context, handler, json_params);
    }

    public RMGetApiRequest(String url, Context context, Handler handler, String username, String password)
    {
        super(RMApiClient.c_Get, url, context, handler, username, password);
    }
}
