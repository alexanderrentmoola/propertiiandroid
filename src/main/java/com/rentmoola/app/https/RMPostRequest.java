package com.rentmoola.app.https;
import android.content.Context;
import android.os.Handler;

import java.util.HashMap;

public class RMPostRequest extends RMClient
{
    public RMPostRequest(String url, Context context, Handler handler, String json_params)
    {
        super(RMClient.c_Post, url, context, handler, json_params);
    }
}
