package com.rentmoola.app.https;

import android.content.Context;
import android.os.Handler;

import java.net.CookieHandler;
import java.net.CookieManager;

public class RMGetRequest extends RMClient
{
    public RMGetRequest(String url, Context context, Handler handler, String json_params)
    {
        super(RMClient.c_Get, url, context, handler, json_params);
    }

    public RMGetRequest(String url, Context context, Handler handler, String username, String password)
    {
        super(RMClient.c_Get, url, context, handler, username, password);
    }
}
