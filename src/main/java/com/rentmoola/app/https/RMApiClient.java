package com.rentmoola.app.https;
import android.content.Context;
import android.os.Handler;

import org.json.JSONObject;

import java.io.BufferedWriter;
import java.io.IOException;
import java.io.OutputStreamWriter;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.SocketException;
import java.util.HashMap;

public class RMApiClient extends RMClient
{
    protected String m_ApiToken;

    public RMApiClient(int verb, String url, Context context, Handler handler, String json_params)
    {
        super(verb, url, context, handler, json_params);
    }

    public RMApiClient(int verb, String url, Context context, Handler handler, HashMap<String, String> params)
    {
        super(verb, url, context, handler, params);
    }

    RMApiClient(int verb, String url, Context context, Handler handler, String username, String password)
    {
        super(verb, url, context, handler, username, password);
    }

    @Override
    public void SetAuthorization()
    {
        m_ApiToken = ApiActivity.GetApiToken();
        if(m_ApiToken != null)
        {
            m_UrlConnection.setRequestProperty("Authorization", "Bearer " + m_ApiToken);
            m_UrlConnection.setRequestProperty("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
            m_UrlConnection.setRequestProperty("Access-Control-Allow-Headers", "Content-Type, Authorization, Accept");
            m_UrlConnection.setRequestProperty("Access-Control-Allow-Origin", "*");
            m_UrlConnection.setRequestProperty("Connection","Close");
        }
    }

    @Override
    protected void Post() throws IOException
    {
        try
        {
            m_ResponseString = "";

            createConnection("POST");

            SetAuthorization();
            String post_data;
            m_OutputStream = m_UrlConnection.getOutputStream();
            m_BufferedWriter = new BufferedWriter(new OutputStreamWriter(m_OutputStream, "UTF-8"));

            JSONObject obj = new JSONObject(m_JSONParams);
            // post_data = URLEncoder.encode(obj.toString());
            post_data = obj.toString();
            if(post_data.length() > 0) {
                // m_UrlConnection.setFixedLengthStreamingMode(post_data.length());
            }

            m_BufferedWriter.write(post_data);

            try {
                m_BufferedWriter.flush();
            } catch (SocketException e) {
                e.printStackTrace();
            }

            m_LastResponseCode = m_UrlConnection.getResponseCode();
            m_ResponseString = readStream(m_UrlConnection.getInputStream());

        }
        catch (SocketException e) {
            m_ResponseString = readStream(m_UrlConnection.getErrorStream());
            ShutdownConnection();
        }
        catch(Exception ex) {
            m_ResponseString = readStream(m_UrlConnection.getErrorStream());
            ShutdownConnection();
        }
        finally {
            ShutdownConnection();
        }
    }
}
