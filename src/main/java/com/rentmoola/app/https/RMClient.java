package com.rentmoola.app.https;

import android.content.Context;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import org.json.JSONObject;

import com.rentmoola.app.R;
import com.rentmoola.core.exceptions.RmException;
import com.rentmoola.core.util.RmHttpUtil;
import com.rentmoola.core.util.RmUtil;

/*
import com.rentmoola.core.RmRecordTypes;
import com.rentmoola.core.model.RmModel;
import com.rentmoola.core.model.user.RmLandlord;
import com.rentmoola.core.util.RmHttpUtil;
*/

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.ByteArrayOutputStream;
import java.io.DataOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.OutputStreamWriter;
import java.io.PrintWriter;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.net.CookieManager;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.SocketException;
import java.net.URL;
import java.net.URLEncoder;
import java.security.KeyManagementException;
import java.security.KeyStore;
import java.security.NoSuchAlgorithmException;
import java.security.cert.CertificateException;
import java.util.HashMap;
import java.util.Iterator;
import java.util.Map;

import javax.net.ssl.HostnameVerifier;
import javax.net.ssl.HttpsURLConnection;
import javax.net.ssl.SSLContext;
import javax.net.ssl.SSLSession;
import javax.net.ssl.SSLSocketFactory;
import javax.net.ssl.TrustManager;
import javax.net.ssl.TrustManagerFactory;
import javax.net.ssl.X509TrustManager;

import org.apache.commons.codec.binary.Base64;

import okhttp3.OkHttpClient;

import static com.rentmoola.app.BuildConfig.DEBUG;

public class RMClient implements Runnable
{
    static public final int c_InvalidRESTVerb = -1;
    static public final int c_Get = 0;
    static public final int c_Post = 1;
    static public final int c_Put = 2;
    static public final int c_Delete = 3;

    protected int m_RESTVerb;
    protected Handler m_Handler;
    protected String m_ResponseString;
    protected String m_JSONParams;
    protected String m_Username;
    protected String m_Password;
    protected String m_Token;

    protected HashMap<String, String> m_Params = new HashMap<String, String>();

    protected Context m_Context;
    protected String m_RequestUrl;
    protected URL m_Url;
    protected int m_LastResponseCode;
    protected final String USER_AGENT = "Mozilla/5.0";

    protected HttpURLConnection m_UrlConnection = null;

    protected Thread m_ApiThread;
    protected OutputStream m_OutputStream;
    protected BufferedWriter m_BufferedWriter;

    static protected SSLSocketFactory m_SSLSocketFactory;

    RMClient(int verb, String url, Context context, Handler handler, String json_params)
    {
        m_RESTVerb = verb;
        m_RequestUrl = url;
        m_Handler = handler;
        m_Context = context;
        m_JSONParams = json_params;
        CookieHandler.setDefault(new CookieManager());
    }

    RMClient(int verb, String url, Context context, Handler handler, HashMap<String, String> params)
    {
        m_RESTVerb = verb;
        m_RequestUrl = url;
        m_Handler = handler;
        m_Context = context;
        m_Params = params;
        CookieHandler.setDefault(new CookieManager());
    }

    RMClient(int verb, String url, Context context, Handler handler, String username, String password)
    {
        m_RESTVerb = verb;
        m_RequestUrl = url;
        m_Handler = handler;
        m_Context = context;
        m_Username = username;
        m_Password = password;
        CookieHandler.setDefault(new CookieManager());
    }

    public void Start()
    {
        m_ApiThread = new Thread(this);
        m_ApiThread.start();
    }

    public int GetLastResponseCode()
    {
        return m_LastResponseCode;
    }

    @Override
    public void run()
    {
        m_LastResponseCode = 401;

        try
        {
            if(m_RESTVerb != c_InvalidRESTVerb)
            {
                switch(m_RESTVerb)
                {
                    case(c_Get):
                        Get();
                        break;

                    case(c_Post):
                        Post();
                        break;

                    case(c_Put):
                        Put();
                        break;
                }
            }


            // if(m_LastResponseCode != 999)
            {
                SendResponse();
            }
            /*
            else
            {
                Log.v("ApiClient", "Response code:" + m_LastResponseCode);
            }
            */
        }
        catch (Throwable ex)
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintWriter writer = new PrintWriter(baos);
            ex.printStackTrace(writer);
            writer.flush();
            writer.close();
        }
    }

    public void SetRequestHeader(String field, String value)
    {
        m_UrlConnection.setRequestProperty (field, value);
    }

    public String GetResponse()
    {
        return m_ResponseString;
    }

    protected void createConnection(String method) throws Exception
    {
        m_Url = new URL(m_RequestUrl);
        m_UrlConnection = (HttpURLConnection) m_Url.openConnection();
        m_UrlConnection.setRequestMethod(method);

        if(method.equals("PUT") || method.equals("POST"))
        {
            // m_UrlConnection.setRequestProperty("connection", "close"); // disables Keep Alive

            m_UrlConnection.setDoOutput(true);
            m_UrlConnection.setRequestProperty("Accept", "application/json");
        }

        m_UrlConnection.setConnectTimeout(1500);
        m_UrlConnection.setReadTimeout(1500);
        m_UrlConnection.setRequestProperty("User-Agent", USER_AGENT);
        m_UrlConnection.setRequestProperty("Content-Type", "application/json");

        /*
        if(DEBUG) {
            if (m_UrlConnection instanceof HttpsURLConnection) {
                ((HttpsURLConnection) m_UrlConnection).setSSLSocketFactory(newSslSocketFactory(m_Context));
            }

            HttpsURLConnection https_connection = (HttpsURLConnection) m_UrlConnection;
            https_connection.setHostnameVerifier(new HostnameVerifier() {
                @Override
                public boolean verify(String hostname, SSLSession session) {
                    return true;
                }
            });
        }
        */
    }

    protected void Post() throws IOException
    {
        try
        {
            createConnection("POST");

            SetAuthorization();
            String post_data;
            m_OutputStream = m_UrlConnection.getOutputStream();
            m_BufferedWriter = new BufferedWriter(new OutputStreamWriter(m_OutputStream, "UTF-8"));

            JSONObject obj = new JSONObject(m_JSONParams);
            // post_data = URLEncoder.encode(obj.toString());
            post_data = obj.toString();
            if(post_data.length() > 0) {
               //  m_UrlConnection.setFixedLengthStreamingMode(post_data.length());
            }

            m_BufferedWriter.write(post_data);

            /*
            if(m_EncodePOSTDataAsJSON)
            {
                JSONObject obj = new JSONObject(m_Params);
                // post_data = URLEncoder.encode(obj.toString());
                post_data = obj.toString();
                writer.write(post_data);
            }
            else
            {
                post_data = GetPostDataString(m_Params);
                writer.write(post_data);
            }
            */

            try {
                m_BufferedWriter.flush();
            } catch (SocketException e) {
                e.printStackTrace();
                /* insert your failure processings here */
            }

            try {
                m_BufferedWriter.close();
            } catch (SocketException e) {
                e.printStackTrace();
                /* insert your failure processings here */
            }

            try {
                m_OutputStream.close();
            } catch (SocketException e) {
                e.printStackTrace();
                /* insert your failure processings here */
            }

            m_LastResponseCode = m_UrlConnection.getResponseCode();
            m_ResponseString = readStream(m_UrlConnection.getInputStream());

        } catch(Exception ex) {
            m_ResponseString = readStream(m_UrlConnection.getErrorStream());
            m_OutputStream.close();
            m_BufferedWriter.close();
        }
        finally {
            if(m_UrlConnection != null) {
                m_UrlConnection.disconnect();
            }
        }
    }

    public String Get() throws Exception
    {
        String result = null;

        try
        {
            createConnection("GET");

            // Set Authorization
            if((m_Username != null) && (m_Password != null))
            {
                StringBuilder header = (new StringBuilder()).append(m_Username).append(':').append(m_Password);
                String authorization = null;
                try
                {
                    authorization = "Basic " + java.util.Base64.getEncoder().encodeToString(header.toString().getBytes("UTF-8"));
                } catch (UnsupportedEncodingException var4)
                {
                    throw new RmException(var4);
                }
                m_UrlConnection.setRequestProperty("Authorization", authorization);
            }
            else
            {
                // SetAuthorization();
            }

            m_LastResponseCode = m_UrlConnection.getResponseCode();
            m_ResponseString = readStream(m_UrlConnection.getInputStream());

        } catch(Exception ex) {
            result = ex.toString();
            m_ResponseString = readStream(m_UrlConnection.getErrorStream());
        }
        finally {
            if(m_UrlConnection != null) {
                m_UrlConnection.disconnect();
            }
        }
        return result;
    }

    protected void Put() throws IOException
    {
        try
        {
            createConnection("PUT");

            SetAuthorization();

            OutputStreamWriter writer = new OutputStreamWriter(m_UrlConnection.getOutputStream());
            writer.write(m_JSONParams);
            writer.flush();
            writer.close();
            m_LastResponseCode = m_UrlConnection.getResponseCode();
            m_ResponseString = readStream(m_UrlConnection.getInputStream());
        } catch(Exception ex) {
            m_ResponseString = readStream(m_UrlConnection.getErrorStream());
        }
        finally {
            if(m_UrlConnection != null) {
                m_UrlConnection.disconnect();
            }
        }
    }


    protected void SendResponse()
    {
        Message msg = new Message();
        Bundle b = new Bundle();

        String lowerCaseUrl = m_Url.toString().toLowerCase();
        b.putString("request_url", m_RequestUrl);
        b.putString("http_code", new Integer(m_LastResponseCode).toString());
        if(m_ResponseString != null)
        {
            b.putString("apiResponse", m_ResponseString);
        }

        msg.setData(b);
        m_Handler.sendMessage(msg);
    }

    protected String GetPostDataString(HashMap<String, String> params) throws UnsupportedEncodingException
    {
        StringBuilder result = new StringBuilder();
        boolean first = true;
        for(Map.Entry<String, String> entry : params.entrySet()){
            if (first)
                first = false;
            else
                result.append("&");

            result.append(URLEncoder.encode(entry.getKey(), "UTF-8"));
            result.append("=");
            result.append(URLEncoder.encode(entry.getValue(), "UTF-8"));
        }

        return result.toString();
    }

    protected String readStream(InputStream in)
    {
        BufferedReader reader = null;
        StringBuffer response = new StringBuffer();
        try {
            reader = new BufferedReader(new InputStreamReader(in));
            String line = "";
            while ((line = reader.readLine()) != null) {
                response.append(line);
            }
        } catch (IOException e) {
            e.printStackTrace();
        } finally {
            if (reader != null) {
                try {
                    reader.close();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
        }
        return response.toString();
    }

    public void SetEncodePOSTDataAsJSON(boolean encode_as_json)
    {
       //  m_EncodePOSTDataAsJSON = encode_as_json;
    }

    public void SetAuthorization()
    {
        m_Token = ApiActivity.GetToken();
        if(m_Token != null)
        {
            m_UrlConnection.setRequestProperty("Authorization", "Bearer " + m_Token);
            // m_UrlConnection.setRequestProperty("Authorization", m_Token);
            m_UrlConnection.setRequestProperty("Access-Control-Allow-Methods", "GET, POST, PUT, DELETE, OPTIONS");
            m_UrlConnection.setRequestProperty("Access-Control-Allow-Headers", "Content-Type, Authorization, Accept");
            m_UrlConnection.setRequestProperty("Access-Control-Allow-Origin", "*");
        }
    }


    protected SSLSocketFactory newSslSocketFactory(Context context)
    {
        // Create a trust manager that does not validate certificate chains
        final TrustManager[] trustAllCerts = new TrustManager[]{
                new X509TrustManager() {
                    @Override
                    public void checkClientTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public void checkServerTrusted(java.security.cert.X509Certificate[] chain, String authType) throws CertificateException {
                    }

                    @Override
                    public java.security.cert.X509Certificate[] getAcceptedIssuers() {
                        return null;
                    }
                }
        };

        // Install the all-trusting trust manager
        SSLContext sslContext = null;
        try
        {
            sslContext = SSLContext.getInstance("SSL");
        } catch (NoSuchAlgorithmException e)
        {
            e.printStackTrace();
        }
        try
        {
            sslContext.init(null, trustAllCerts, new java.security.SecureRandom());
        } catch (KeyManagementException e)
        {
            e.printStackTrace();
        }
        // Create an ssl socket factory with our all-trusting manager
        final SSLSocketFactory sslSocketFactory = sslContext.getSocketFactory();
        return sslSocketFactory;
    }

    protected void ShutdownConnection()
    {
        if(m_OutputStream != null) {
            try {
                m_OutputStream.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            m_OutputStream = null;
        }

        if(m_BufferedWriter != null) {
            try {
                m_BufferedWriter.close();
            } catch (IOException e) {
                e.printStackTrace();
            }
            m_BufferedWriter = null;
        }

        if(m_UrlConnection != null)
        {
            m_UrlConnection.disconnect();
            m_UrlConnection = null;
        }
    }
}
