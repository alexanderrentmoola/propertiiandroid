package com.rentmoola.app.https;

import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.support.v7.app.AppCompatActivity;
import android.util.Base64;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;

// import com.blockchainacuity.wallet.R;
// import com.blockchainacuity.wallet.ui.AbstractBindServiceActivity;
// import com.blockchainacuity.wallet.ui.LoginActivity;

import com.rentmoola.app.BuildConfig;
import com.rentmoola.app.SigninActivity;
import com.rentmoola.app.SplashActivity;
import com.rentmoola.core.model.user.RmLandlord;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.InputStream;
import java.io.UnsupportedEncodingException;
import java.util.HashMap;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rentmoola.core.model.RmModel;
import com.rentmoola.core.model.user.RmAdmin;
import com.rentmoola.core.model.user.RmCustomer;
import com.rentmoola.core.model.user.RmLandlord;
import com.rentmoola.core.util.RmHttpUtil;

import static com.rentmoola.app.BuildConfig.*;


public class ApiActivity extends AppCompatActivity
{
    private ProgressDialog mProgressDialog;
    protected ApiThreadHandler m_ApiThreadHandler;
    protected JSONObject m_ResponseJson;
    protected RmModel m_SessionRole;

    static protected SharedPreferences m_LoginPrefs;
    static public final String m_LoginPrefsFile = "LoginDetails";

    static protected RmLandlord m_Landlord;
    static public final String m_LandlordDataFilename = "LandlordData";
    protected SharedPreferences m_LandlordFile;
    protected RMPostRequest m_RMPostRequest;
    static protected HashMap<String, Object> m_LandlordCustomFields = new HashMap<String, Object>();

    static protected String m_Token;
    static protected String m_ApiToken;

    static public String m_ApiDomain = "https://react-test-api.rentmoola.com/";
    static public String m_SiteBaseDomain = "https://react-test.rentmoola.com/";
    static public String m_SiteDomain = "https://react-test.rentmoola.com/";

    static protected String m_Cookie;


    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        /*
        if(DEBUG)
        {
            m_ApiDomain = "http://192.168.0.130:8080/";
            m_SiteBaseDomain = "http://192.168.0.130";
            m_SiteDomain = m_SiteBaseDomain + ":3000/";

            // m_ApiDomain = "http://192.168.0.159:8080/";
            // m_SiteDomain = "http://192.168.0.159:3000/";

            // m_ApiDomain = "http://192.168.0.118:8080/";
            // m_SiteDomain = "http://192.168.0.118:3000/";
        }
        */

    }

    static public String GetToken()
    {
        return m_Token;
    }

    static public String GetApiToken()
    {
        return m_ApiToken;
    }

    public void ClearLandlordFaceRecCustomFields()
    {
        m_LandlordCustomFields.put("face_rec_score", "");
        m_LandlordCustomFields.put("card_face_image", "");
        m_LandlordCustomFields.put("live_face_image", "");

        SaveLandlord();
    }

    public void ClearLandlordIdCheckCustomFields()
    {
        m_LandlordCustomFields.put("Canadian_Citizen_File", "");
        m_LandlordCustomFields.put("International_Watchlist", "");
        m_LandlordCustomFields.put("Passport_MRZ_Validation", "");
        m_LandlordCustomFields.put("Canadian_SIN_Validation", "");
        m_LandlordCustomFields.put("Canada_Credit_Agency", "");

        SaveLandlord();
    }

    public void AddToLandlordJSON(final String key, final String value)
    {
        m_LandlordCustomFields.put(key, value);
    }

    public boolean IsFirstRun()
    {
        if(m_LoginPrefs.getBoolean("firstrun", true))
        {
            // Do first run stuff here then set 'firstrun' as false
            // using the following line to edit/commit prefs
            m_LoginPrefs.edit().putBoolean("firstrun", false).commit();
            return true;
        }
        return false;
    }

    public boolean IsSignupEmailSent()
    {
        return m_LoginPrefs.getBoolean("signup_email_ssnt", false);
    }

    public boolean IsUserSignedUp()
    {
        return m_LoginPrefs.getBoolean("user_signed_up", false);
    }

    public boolean IsUserIdentityChecked()
    {
        return m_LoginPrefs.getBoolean("identity_checked", false);
    }

    public void handleRequestResponse(ApiResponse apiResponse){
        // mLastRequestResponse = apiResponse;

       // CloseProgressDialog();
    }

    protected void SaveLandlord(String landlord_json)
    {
        System.out.println("Landlord JSON:");
        System.out.println(landlord_json);

        final String url = ApiActivity.m_ApiDomain + "update";
        ApiThreadHandler handler = new ApiThreadHandler(this);

        m_LandlordFile = getSharedPreferences(m_LandlordDataFilename, Context.MODE_PRIVATE);
        SharedPreferences.Editor editor = m_LandlordFile.edit();
        editor.putString("landlord_json", landlord_json);
        editor.commit();

        m_RMPostRequest = new RMPostRequest(url, this, handler, landlord_json);
        m_RMPostRequest.Start();
    }

    public void SaveLandlord()
    {
        if(m_Landlord != null)
        {
            m_Landlord.setCustomFields(m_LandlordCustomFields);
            final String landlord_json = m_Landlord.toJson();

            SaveLandlord(landlord_json);
        }
    }

    protected RmLandlord LoadLandlord()
    {
        m_LandlordFile = getSharedPreferences(m_LandlordDataFilename, Context.MODE_PRIVATE);
        final String landlord_json = m_LandlordFile.getString("landlord_json", "");

        if(landlord_json.length() > 0)
        {
            m_Landlord = (RmLandlord) RmLandlord.fromJson(landlord_json);
        }
        return m_Landlord;
    }

    public void StartSigninActivity()
    {
        // intent = new Intent(m_Activity, WebViewActivity.class);
        Intent intent = new Intent(this, SigninActivity.class);
        intent.putExtra("url",  "login");
    }

    protected String GetTokenFromResponse(String response_text)
    {
        String split_jwt_token[] = null;

        final String token  = response_text;
        split_jwt_token = token.split("\\.");

        String decoded_jwt_token = null;
        byte[] data = Base64.decode(split_jwt_token[1], Base64.DEFAULT);
        try
        {
            decoded_jwt_token = new String(data, "UTF-8");
        } catch (UnsupportedEncodingException e)
        {
            e.printStackTrace();
            return null;
        }

        JSONObject landlord_json = null;
        try
        {
            landlord_json = new JSONObject(decoded_jwt_token);
        } catch (JSONException e)
        {
            e.printStackTrace();
            return null;
        }

        try
        {
            landlord_json.put("type", "TYPE_LANDLORD");
        } catch (JSONException e)
        {
            e.printStackTrace();
            return null;
        }

        JsonNode jsonObject = null;
        ObjectMapper mapper = new ObjectMapper();
        mapper.configure(DeserializationFeature.FAIL_ON_UNKNOWN_PROPERTIES, false);
        try
        {
            jsonObject = mapper.readTree(String.valueOf(landlord_json));
        } catch (IOException e)
        {
            e.printStackTrace();
            return null;
        }

        // if(m_LoginPrefs.getBoolean("user_signed_up", false))
        if(m_LoginPrefs != null)
        {
            m_LoginPrefs.edit().putBoolean("user_signed_up", true).commit();
        }

        JsonNode node = jsonObject.get("sessionRole");
        m_SessionRole = RmModel.fromJson(node.asText());
        return token;
    }

}
