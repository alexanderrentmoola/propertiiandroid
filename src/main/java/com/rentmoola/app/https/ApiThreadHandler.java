package com.rentmoola.app.https;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;


public class ApiThreadHandler extends Handler
{
    private ApiActivity m_Activity;

    int plap = 0;

    public ApiThreadHandler(ApiActivity activity)
    {
        m_Activity = activity;
    }

    @Override
    public void handleMessage(Message msg) {

        Bundle bundle =	msg.getData();
        String request_url = bundle.getString("request_url");
        String apiResponseString = bundle.getString("apiResponse");
        String http_code = bundle.getString("http_code");

        ApiResponse apiResponse = new ApiResponse(request_url, http_code, apiResponseString);
        if(m_Activity != null)
        {
            m_Activity.handleRequestResponse(apiResponse);
        }
        else
        {
            plap = 678;
        }
    }
}
