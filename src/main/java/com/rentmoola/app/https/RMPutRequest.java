package com.rentmoola.app.https;
import android.content.Context;
import android.os.Handler;

import java.util.HashMap;

public class RMPutRequest extends RMClient
{
    public RMPutRequest(String url, Context context, Handler handler, String json_params)
    {
        super(RMClient.c_Put, url, context, handler, json_params);
    }

    public RMPutRequest(String url, Context context, Handler handler, HashMap<String, String> json_params)
    {
        super(RMClient.c_Put, url, context, handler, json_params);
    }


}
