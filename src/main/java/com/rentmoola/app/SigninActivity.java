package com.rentmoola.app;

import android.app.Activity;
import android.content.Intent;
import android.os.Bundle;
import android.util.Base64;

import android.widget.Button;
import android.widget.EditText;
import android.widget.TextView;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rentmoola.app.https.ApiActivity;
// import com.rentmoola.app.https.ApiPostRequest;
// import com.rentmoola.app.https.ApiThreadHandler;
import com.rentmoola.app.https.ApiResponse;
import com.rentmoola.app.https.ApiThreadHandler;
import com.rentmoola.app.https.RMGetRequest;
import com.rentmoola.app.https.RMPostRequest;
import com.rentmoola.core.model.RmModel;
import com.rentmoola.core.model.user.RmAdmin;
import com.rentmoola.core.model.user.RmCustomer;
import com.rentmoola.core.model.user.RmLandlord;
import com.rentmoola.core.util.RmHttpUtil;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

import static com.rentmoola.core.model.user.RmLandlord.APPLICATION_STATUS.IDENTITY_CONFIRMED;

public class SigninActivity extends ApiActivity
{
    Button b1,b2;
    EditText ed1,ed2;

    TextView tx1;
    int counter = 3;
    Activity m_Activity;

    private RMGetRequest m_RMGetRequest;
    private String m_Url;

    @Override
    public void handleRequestResponse(ApiResponse api_response)
    {
        final String response = api_response.GetResponseString();
        final int response_code = api_response.GetResponseCode();
        if(response_code == 200)
        {
            if(m_Url.contains("/authorize"))
            {
                HandleSigninResponse(response);
            }
        }
    }

    private void HandleSigninResponse(String response)
    {
        try
        {
            m_ResponseJson = new JSONObject(response);
        } catch (JSONException e)
        {
            e.printStackTrace();
            return;
        }

        try
        {
            m_Token = GetTokenFromResponse(m_ResponseJson.getString("access_token"));
            if(m_Token != null)
            {
                // if(!IsUserIdentityChecked() /*&& !started_main*/)
                {
                    // started_main = true;

                    // ClearLandlordCustomFields();

                    m_Landlord = (RmLandlord)m_SessionRole;
                    SaveLandlord();
                    // m_Activity.finish();

                    System.out.println("STARTING MAIN!");

                    Intent intent = new Intent(m_Activity, MainActivity.class);
                    startActivity(intent);

                    // Intent intent = new Intent(m_Activity, IdScanResultActivity.class);
                    //  startActivity(intent);

                    // Intent intent = new Intent(m_Activity, LandlordCheckActivity.class);
                    // startActivity(intent);
                }
                /*
                else
                {
                    Intent intent = new Intent(m_Activity, WebViewActivity.class);
                    intent.putExtra("url",  "login");
                    startActivity(intent);

                    // Intent intent = new Intent(m_Activity, MainActivity.class);
                    // startActivity(intent);
                }
                */
            }
        } catch (JSONException e) {
            e.printStackTrace();
        }
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.signin);

        m_Activity = this;

        b1 = (Button)findViewById(R.id.button);
        ed1 = (EditText)findViewById(R.id.nameEdit);
        ed2 = (EditText)findViewById(R.id.passwordEdit);

        // ed1.setText("david@appsourcer.com");
        // ed2.setText("Password1");

        // DoLoginRequest("mail@alexanderhoughton.com", "Password1");
        // DoLoginRequest("david@appsourcer.com", "Password1");
        // DoLoginRequest("alexanderhoughtonca@gmail.com", "Password1");
        // DoLoginRequest("kevin+user@rentmoola.com", "Moola2012");

        DoLoginRequest(ed1.getText().toString(), ed2.getText().toString());

    }

    public void DoLoginRequest(String username, String password)
    {
        m_Url = ApiActivity.m_ApiDomain + "authorize";
        m_ApiThreadHandler = new ApiThreadHandler(this);
        m_RMGetRequest = new RMGetRequest(m_Url, this, m_ApiThreadHandler, username, password);
        m_RMGetRequest.Start();
    }

    @Override
    protected void onResume()
    {
        super.onResume();
    }
}