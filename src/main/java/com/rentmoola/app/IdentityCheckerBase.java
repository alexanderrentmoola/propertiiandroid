package com.rentmoola.app;

import android.os.Bundle;
import android.os.Handler;
import android.os.Message;

import com.rentmoola.app.https.ApiActivity;
import com.rentmoola.app.https.ApiResponse;
import com.rentmoola.app.https.ApiThreadHandler;

import java.io.ByteArrayOutputStream;
import java.io.PrintWriter;

public class IdentityCheckerBase extends Handler implements Runnable
{
    protected String m_Username;
    protected String m_Password;
    protected boolean m_Connected;
    protected ApiThreadHandler m_ApiThreadHandler;
    private ApiActivity m_Activity;
    protected int m_LastResponseCode;
    protected String m_ResponseString;

    protected Thread m_Thread;

    @Override
    public void handleMessage(Message msg)
    {
        Bundle bundle =	msg.getData();
        String request_url = bundle.getString("request_url");
        String apiResponseString = bundle.getString("apiResponse");
        String http_code = bundle.getString("http_code");

        ApiResponse apiResponse = new ApiResponse(request_url, http_code, apiResponseString);
        if(m_Activity != null)
        {
            m_Activity.handleRequestResponse(apiResponse);
        }
    }

    protected void SendResponse()
    {
        Message msg = new Message();
        Bundle b = new Bundle();

        // String lowerCaseUrl = m_Url.toString().toLowerCase();
        // b.putString("request_url", m_RequestUrl);
        b.putString("http_code", new Integer(m_LastResponseCode).toString());
        if(m_ResponseString != null)
        {
            b.putString("apiResponse", m_ResponseString);
        }

        msg.setData(b);
        sendMessage(msg);
    }

    public IdentityCheckerBase(ApiActivity activity, String username, String password)
    {
        m_Username = username;
        m_Password = password;
        m_Activity = activity;
    }

    public void Login()
    {
    }

    public void Start()
    {
        m_Thread = new Thread(this);
        m_Thread.start();
    }

    /*
    public int GetLastResponseCode()
    {
        return m_LastResponseCode;
    }
    */

    @Override
    public void run()
    {
        Login();

        try
        {
            /*
            if(m_RESTVerb != c_InvalidRESTVerb)
            {
                switch(m_RESTVerb)
                {
                    case(c_Get):
                        Get();
                        break;

                    case(c_Post):
                        Post();
                        break;

                    case(c_Put):
                        Put();
                        break;
                }
            }

            if(m_LastResponseCode != 999)
            {
                SendResponse();
            }
            */
        }
        catch (Throwable ex)
        {
            ByteArrayOutputStream baos = new ByteArrayOutputStream();
            PrintWriter writer = new PrintWriter(baos);
            ex.printStackTrace(writer);
            writer.flush();
            writer.close();
        }
    }

}
