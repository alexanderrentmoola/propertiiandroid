package com.rentmoola.app;

import android.app.Activity;
import android.content.Context;
import android.content.Intent;
import android.graphics.Bitmap;
import android.net.http.SslError;
import android.os.Build;
import android.os.Bundle;
import android.os.SystemClock;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.View;
import android.webkit.CookieManager;
import android.webkit.CookieSyncManager;
import android.webkit.SslErrorHandler;
import android.webkit.ValueCallback;
import android.webkit.WebSettings;
import android.webkit.WebView;
import android.webkit.WebViewClient;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.rentmoola.app.https.ApiActivity;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.io.UnsupportedEncodingException;
import java.net.CookieHandler;
import java.util.Base64;
import java.util.List;

import com.fasterxml.jackson.databind.DeserializationFeature;
import com.fasterxml.jackson.databind.JsonNode;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.rentmoola.core.model.RmModel;
import com.rentmoola.core.model.user.RmAdmin;
import com.rentmoola.core.model.user.RmCustomer;
import com.rentmoola.core.model.user.RmLandlord;
import com.rentmoola.core.util.RmHttpUtil;

import okhttp3.Cookie;

public class WebViewActivity extends ApiActivity
{
    Button b1;
    EditText ed1;

    private WebView web_view;
    private boolean m_IsLocalUrl = false;

    protected String m_Url;
    private boolean started_main = false;

    private CookieSyncManager mSyncManager;
    private CookieManager m_CookieManager;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.web_view);

        web_view=(WebView)findViewById(R.id.webView);
        web_view.setWebViewClient(new MyBrowser(this));

        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.KITKAT) {
            WebView.setWebContentsDebuggingEnabled(true);
        }

        // FOR DEV ONLY!!!
        web_view.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_COMPATIBILITY_MODE);
        web_view.getSettings().setMixedContentMode(WebSettings.MIXED_CONTENT_ALWAYS_ALLOW);

        web_view.getSettings().setAllowFileAccessFromFileURLs(true);
        web_view.getSettings().setAllowUniversalAccessFromFileURLs(true);
        web_view.getSettings().setLoadsImagesAutomatically(true);
        web_view.getSettings().setJavaScriptEnabled(true);
        web_view.setScrollBarStyle(View.SCROLLBARS_INSIDE_OVERLAY);

        if ( Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP )
        {
            m_CookieManager = CookieManager.getInstance();
            m_CookieManager.setAcceptThirdPartyCookies( web_view, true );
            m_CookieManager.setAcceptCookie(true);
            // m_CookieManager.removeSessionCookie();
        }

        String url = getIntent().getStringExtra( "url");
        if(url.contains("192.168.0") || !url.contains(".com"))
        {
            m_IsLocalUrl = true;
            m_Url = m_SiteDomain + url;
        }
        else
        {
            m_Url = url;
        }

        // web_view.loadUrl("https://www.tutorialspoint.com/android/android_webview_layout.htm");
        // web_view.loadUrl("https://192.168.0.130:3000/login");
        web_view.loadUrl(m_Url);

        WebSettings settings = web_view.getSettings();
        settings.setDomStorageEnabled(true);
        settings.setDatabaseEnabled(true);

        if (Build.VERSION.SDK_INT < Build.VERSION_CODES.KITKAT) {
            File databasePath = getDatabasePath("yourDbName");
            settings.setDatabasePath(databasePath.getPath());
        }

        // Give Android time to process cookies
        SystemClock.sleep(1000);
    }

    private class MyBrowser extends WebViewClient {

        private ApiActivity m_Activity;

        public MyBrowser(ApiActivity activity){
            m_Activity = activity;

            ClearCookies();
        }

        private void ClearCookies()
        {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP)
            {
                m_CookieManager = CookieManager.getInstance();
                m_CookieManager.removeAllCookies(new ValueCallback<Boolean>()
                {
                    // a callback which is executed when the cookies have been removed
                    @Override
                    public void onReceiveValue(Boolean aBoolean) {
                        Log.d("0", "Cookie removed: " + aBoolean);
                    }
                });
            }
            else
            {
                m_CookieManager.removeAllCookie();
            }
        }

        @Override
        public void onReceivedSslError(WebView view, SslErrorHandler handler, SslError error) {
            super.onReceivedSslError(view, handler, error);

            // this will ignore the Ssl error and will go forward to your site
            handler.proceed();
        }

        @Override
        public boolean shouldOverrideUrlLoading(WebView view, String url) {
            view.loadUrl(url);
            return true;
        }

        public void onLoadResource(WebView view, String url){
            if(CheckForCookie(url)){
                Toast.makeText(m_Activity, "YAY - onLoadResource!", Toast.LENGTH_LONG).show();
            }
        }

        @Override
        public void onPageFinished(WebView view, String url) {
            if(CheckForCookie(url)){
                Toast.makeText(m_Activity, "YAY!", Toast.LENGTH_LONG).show();
            }
        }

        public boolean CheckForCookie(String url)
        {
            m_LoginPrefs = getSharedPreferences(m_LoginPrefsFile, Context.MODE_PRIVATE);
            m_LoginPrefs.edit().putBoolean("signup_email_ssnt", true).commit();

            m_CookieManager.flush();

//             public static Cookie cookie = null;

            // Get the cookie from cookie jar.
            m_Cookie = m_CookieManager.getCookie(m_SiteBaseDomain);
            if (m_Cookie == null)
            {
                return false;
            }

            if(!m_IsLocalUrl)
            {
                return false;
            }

            m_Token = GetTokenFromResponse(m_Cookie);
            if(m_Token != null)
            {
                System.out.println("HAVE TOKEN!");

                final String model_type = m_SessionRole.getType();
                if(model_type.equals("TYPE_LANDLORD") && url.contains("login"))
                {
                    if(!IsUserIdentityChecked() /*&& !started_main*/)
                    {
                        started_main = true;

                        m_Landlord = (RmLandlord)m_SessionRole;
                        SaveLandlord();
                        // m_Activity.finish();

                        System.out.println("STARTING MAIN!");

                        Intent intent = new Intent(m_Activity, MainActivity.class);
                        startActivity(intent);

                        // Intent intent = new Intent(m_Activity, IdScanResultActivity.class);
                        // startActivity(intent);

                        // Intent intent = new Intent(m_Activity, LandlordCheckActivity.class);
                        // startActivity(intent);
                    }
                }
            }
            return false;
        }
    }


}
