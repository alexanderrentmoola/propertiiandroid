package com.rentmoola.app;

import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.v7.app.AppCompatActivity;
import android.view.Window;

import com.rentmoola.app.https.ApiActivity;

class SplashHandler extends Handler
{
    public static final int STOPSPLASH = 0;
    public static final long SPLASHTIME = 3000;
    private SplashActivity m_Activity;

    public SplashHandler(SplashActivity activity)
    {
        m_Activity = activity;
    }

    @Override
    public void handleMessage(Message msg) {
        switch (msg.what)
        {
            case STOPSPLASH:
            {
                Intent intent = null;

                // intent = new Intent(m_Activity, WebViewActivity.class);
                // intent.putExtra("url", "landlordsignup");
                // intent.putExtra("url", "login");

                final boolean signed_up = m_Activity.IsSignupEmailSent();
                if(!m_Activity.IsUserIdentityChecked())
                {
                    // intent = new Intent(m_Activity, WebViewActivity.class);
                    intent = new Intent(m_Activity, SigninActivity.class);
                    intent.putExtra("url",  "login");
                }
                else
                {
                    // intent = new Intent(m_Activity, WebViewActivity.class);
                    // intent.putExtra("url", "landlordsignup");
                    // intent.putExtra("url",  "login");
                    intent = new Intent(m_Activity, SigninActivity.class);
                    intent.putExtra("url",  "login");
                }


                m_Activity.finish();
                m_Activity.startActivity(intent);
            }
            break;
        }
        super.handleMessage(msg);
    }
}

public class SplashActivity extends ApiActivity
{
        static private Handler splashHandler;

        @Override
        public void onCreate(Bundle savedInstanceState)
        {
            super.onCreate(savedInstanceState);

            // Doing this here since Android won't let me put it in ApiActivity.Create
            m_LoginPrefs = getSharedPreferences(m_LoginPrefsFile, Context.MODE_PRIVATE);
            // boolean user_signed_up = m_LoginPrefs.getBoolean("user_signed_up", false);

            splashHandler = new SplashHandler(this);

            this.requestWindowFeature(Window.FEATURE_NO_TITLE);

            setContentView(R.layout.splash);
            Message msg = new Message();
            msg.what = SplashHandler.STOPSPLASH;
            splashHandler.sendMessageDelayed(msg, SplashHandler.SPLASHTIME);
        }


}