package com.rentmoola.app;

import android.content.Intent;
import android.os.Bundle;
import android.os.Handler;
import android.widget.TextView;

import com.rentmoola.app.https.ApiActivity;
import com.rentmoola.app.https.ApiResponse;
import com.rentmoola.app.https.ApiThreadHandler;
import com.rentmoola.app.https.RMPostRequest;
import com.rentmoola.app.https.RMPutRequest;
import com.rentmoola.core.model.user.RmLandlord;

public class LandlordCheckActivity extends FaceRecActivity
{
    DocumentCheckerTrulioo m_DocChecker;
    IdentityCheckerTrulioo m_IdentityCheckerTrulioo;

    TextView test_result;

    @Override
    public void handleRequestResponse(ApiResponse apiResponse)
    {
        final String response = apiResponse.GetResponseString();
        final String request_url = apiResponse.GetRequestURL();

        if(request_url == "CADocuments")
        {
            m_IdentityCheckerTrulioo.Verify();
        }
        else
        if(request_url.contains("landlord_verify"))
        {
            SaveLandlord();

            Intent intent = new Intent(this, DocVerifyThanksActivity.class);
            startActivity(intent);
        }
        /*
        else
        if(request_url.contains("/update"))
        {
            Intent intent = new Intent(this, DocVerifyThanksActivity.class);
            startActivity(intent);
        }
        */
    }

    @Override
    protected void onCreate(Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_landlord_check);

        LoadLandlord();
        ClearLandlordIdCheckCustomFields();

        PlayGifView pGif = (PlayGifView) findViewById(R.id.viewGif);
        pGif.setImageResource(R.drawable.processing);

        // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // setSupportActionBar(toolbar);

        /*
        FloatingActionButton fab = (FloatingActionButton) findViewById(R.id.fab);
        fab.setOnClickListener(new View.OnClickListener()
        {
            @Override
            public void onClick(View view)
            {
                Snackbar.make(view, "Replace with your own action", Snackbar.LENGTH_LONG)
                        .setAction("Action", null).show();
            }
        });
        */


    }

    @Override
    protected void OnIDDocumentLoaded()
    {
        if(m_IdentityCheckerTrulioo == null)
        {
            m_Landlord = LoadLandlord();
            if (m_Landlord != null)
            {
                m_IdentityCheckerTrulioo = new IdentityCheckerTrulioo(m_Landlord, idDocument, this, "RentMoola_Demo_API", "RentM00laTruliooStuff2018^");
                m_IdentityCheckerTrulioo.Start();
            }
        }
    }
}
