package com.rentmoola.app;

import android.util.Log;

import java.io.IOException;
import java.util.List;
import java.util.Map;

import com.appliedrec.ver_ididcapture.data.IDDocument;
import com.trulioo.normalizedapi.*;
import com.trulioo.normalizedapi.api.*;
import com.trulioo.normalizedapi.model.*;

import org.apache.http.HttpEntity;
import org.apache.http.HttpResponse;
import org.apache.http.client.HttpClient;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.conn.scheme.Scheme;
import org.apache.http.conn.scheme.SchemeRegistry;
import org.apache.http.conn.ssl.SSLSocketFactory;
import org.apache.http.impl.client.DefaultHttpClient;
import org.apache.http.impl.conn.SingleClientConnManager;
import org.apache.http.params.BasicHttpParams;
import org.apache.http.params.HttpParams;
import org.apache.http.util.EntityUtils;

public class DocumentCheckerTrulioo extends DocumentCheckerBase
{
    private ApiClient m_ApiClient;

    protected IDDocument m_FaceRecIdDocument;

    private ConnectionApi m_ConnectionApi;
    private ConfigurationApi m_ConfigurationApi;

    private List<String> m_CountryCodes;
    private Map<String, List<String>> m_CADocumentTypes;
    private Map<String, List<String>> m_USDocumentTypes;

    private VerificationsApi m_VerificationClient;

    public DocumentCheckerTrulioo(IDDocument document, LandlordCheckActivity activity, String username, String password)
    {
        super(activity, username, password);

        m_FaceRecIdDocument = document;
    }

    public void DoVerify( byte[] front_image,  byte[] back_image)
    {
        m_VerificationClient = new VerificationsApi(m_ApiClient);
        VerifyRequest request = new VerifyRequest()
                .acceptTruliooTermsAndConditions(Boolean.TRUE)
                .configurationName("Identity Verification")
                .countryCode("US")
                .dataFields(new DataFields()
                        .personInfo(new PersonInfo()
                                .firstGivenName("Kyle Antwon")
                                .middleName("")
                                .firstSurName("Hertz"))
                        .document(new Document()
                                .documentFrontImage(front_image)
                                .documentBackImage(back_image)
                                .documentType("DrivingLicence"))
        );

        try {
            m_VerificationClient.verifyAsync(request, new ApiCallback<VerifyResult>() {
                @Override
                public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
                    m_LastResponseCode = statusCode;
                }
                @Override
                public void onSuccess(VerifyResult result, int statusCode, Map<String, List<String>> responseHeaders) {
                    System.out.println(result); //To change body of generated methods, choose Tools | Templates.
                }
                @Override
                public void onUploadProgress(long bytesWritten, long contentLength, boolean done) {
                    //To change body of generated methods, choose Tools | Templates.
                }
                @Override
                public void onDownloadProgress(long bytesRead, long contentLength, boolean done) {
                    //To change body of generated methods, choose Tools | Templates.
                }
            });
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    @Override
    public void Login()
    {
        m_ApiClient = new ApiClient();
        m_ApiClient.setUsername(m_Username);
        m_ApiClient.setPassword(m_Password);
        m_ConnectionApi = new ConnectionApi(m_ApiClient);

        try {
            m_ConnectionApi.testAuthenticationAsync(new ApiCallback<String>()
            {
                @Override
                public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
                    m_LastResponseCode = statusCode;
                }
                @Override
                public void onSuccess(String result, int statusCode, Map<String, List<String>> responseHeaders) {
                    m_LastResponseCode = statusCode;
                    m_ResponseString = result;

                    if(!m_Connected)
                    {
                        m_Connected = true;
                        GetCountryCodes();
                    }
                }
                @Override
                public void onUploadProgress(long bytesWritten, long contentLength, boolean done) {
                }
                @Override
                public void onDownloadProgress(long bytesRead, long contentLength, boolean done) {
                }
            });
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    private void GetCountryCodes()
    {
        m_ConfigurationApi = new ConfigurationApi(m_ApiClient);

        try {
            m_ConfigurationApi.getCountryCodesAsync("Identity Verification", new ApiCallback<List<String>>()
            {
                @Override
                public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
                    Log.e("GET RESPONSE", "FAILED!");
                }
                @Override
                public void onSuccess(List<String> result, int statusCode, Map<String, List<String>> responseHeaders) {
                    System.out.println(result); //To change body of generated methods, choose Tools | Templates.
                    m_CountryCodes = result;

                    m_CADocumentTypes = GetDocuments(m_CountryCodes.get(0));
                    m_USDocumentTypes = GetDocuments(m_CountryCodes.get(1));
                }
                @Override
                public void onUploadProgress(long bytesWritten, long contentLength, boolean done) {
                }
                @Override
                public void onDownloadProgress(long bytesRead, long contentLength, boolean done) {
                }
            });
        } catch (ApiException e) {
            e.printStackTrace();
        }
    }

    private Map<String, List<String>> GetDocuments(final String country_code)
    {
        Map<String, List<String>> documents = null;
        try
        {
            m_ConfigurationApi.getDocumentTypesAsync("AU", new ApiCallback<Map<String, List<String>>>()
            {
                @Override
                public void onFailure(ApiException e, int statusCode, Map<String, List<String>> responseHeaders) {
                    Log.e("GET RESPONSE", "FAILED!");
                }
                @Override
                public void onSuccess(Map<String, List<String>> result, int statusCode, Map<String, List<String>> responseHeaders)
                {
                    System.out.println(result);

                    if(country_code.equals("CA"))
                    {
                        m_CADocumentTypes = result;
                    }
                    else
                    if(country_code.equals("US"))
                    {
                        m_USDocumentTypes = result;
                    }

                    m_ResponseString = "Trulioo is ready!";
                    SendResponse();
                }
                @Override
                public void onUploadProgress(long bytesWritten, long contentLength, boolean done) {
                }
                @Override
                public void onDownloadProgress(long bytesRead, long contentLength, boolean done) {
                }
            });
        } catch (ApiException e) {
            e.printStackTrace();
        }
        return documents;
    }

    public void WikiTest()
    {
        SchemeRegistry schemeRegistry = new SchemeRegistry();
        schemeRegistry.register(new Scheme("https", SSLSocketFactory.getSocketFactory(), 443));

        HttpParams params = new BasicHttpParams();

        SingleClientConnManager mgr = new SingleClientConnManager(params, schemeRegistry);

        HttpClient client = new DefaultHttpClient(mgr, params);
        String getURL = "https://wikipedia.org";
        HttpGet get = new HttpGet(getURL);
        HttpResponse responseGet = null;
        try {
            responseGet = client.execute(get);
        } catch (IOException e) {
            e.printStackTrace();
        }
        HttpEntity resEntityGet = responseGet.getEntity();
        if (resEntityGet != null) {
            //do something with the response
            try {

                String data = EntityUtils.toString(resEntityGet);
                Log.e("GET RESPONSE", data);
            } catch (IOException e) {
                e.printStackTrace();
            }
        }
    }
}
