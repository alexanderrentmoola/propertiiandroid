package com.rentmoola.app;

import org.json.JSONException;
import org.json.JSONObject;

public class FieldData extends JSONObject
{
    public FieldData()
    {
    }

    public FieldData(String name)
    {
        try
        {
            put("name", name);
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public FieldData(String name, String value)
    {
        try
        {
            put(name, value);
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
    }
}