package com.rentmoola.app;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.AsyncTask;
import android.os.Bundle;
import android.os.Handler;
import android.os.Message;
import android.support.design.widget.FloatingActionButton;
import android.support.design.widget.Snackbar;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.View;
import android.widget.Button;
import android.widget.Toast;

import com.appliedrec.ver_id.VerID;
import com.appliedrec.ver_id.VerIDLivenessDetectionIntent;
import com.appliedrec.ver_id.session.VerIDLivenessDetectionSessionSettings;
import com.appliedrec.ver_id.session.VerIDSessionResult;
import com.appliedrec.ver_id.ui.VerIDActivity;
import com.appliedrec.ver_ididcapture.CardOverlayView;
import com.appliedrec.ver_ididcapture.IDCaptureActivity;
import com.appliedrec.ver_ididcapture.VerIDIDCapture;
import com.appliedrec.ver_ididcapture.VerIDIDCaptureIntent;
import com.appliedrec.ver_ididcapture.VerIDIDCaptureSettings;
import com.appliedrec.ver_ididcapture.data.IDDocument;
import com.appliedrec.ver_ididcapture.data.IDDocumentPage;
import com.appliedrec.ver_ididcapture.data.Region;
import com.rentmoola.app.https.ApiActivity;
import com.rentmoola.app.https.ApiResponse;


public class FaceRecActivity extends ApiActivity
{
    // Activity request codes
    protected final static int REQUEST_CODE_CARD = 0;
    protected  final static int REQUEST_CODE_FACE = 1;

    // Collected data
    protected IDDocument idDocument;
    protected VerIDSessionResult verIDSessionResult;
    protected Button liveFaceCompareButton;


    // Extracted card and face images
    protected Bitmap cardFaceImage;
    protected Bitmap liveFaceImage;

    protected void onVerIdLoad()
    {
    }

    protected void OnIDDocumentLoaded()
    {
    }

    public void handleRequestResponse(ApiResponse apiResponse){
    }

    protected void StoreFaceIdComparisonResult(Float score)
    {

    }

    class VerIdMessageHandler extends Handler
    {
        FaceRecActivity m_Activity;

        VerIdMessageHandler(FaceRecActivity activity)
        {
            m_Activity = activity;
        }

        @Override
        public void handleMessage(Message msg)
        {
            if(m_Activity != null)
            {
                Bundle bundle = msg.getData();
                String id_document_load_result = bundle.getString("id_document_load");

                if(id_document_load_result.equals("Loaded"))
                {
                    m_Activity.OnIDDocumentLoaded();
                }
                else
                {
                    m_Activity.onVerIdLoad();
                }
            }
        }
    }

    class VerIdLoadCallback implements VerID.LoadCallback
    {
        Bundle m_SavedInstanceState;
        Handler m_MessageHandler;
        FaceRecActivity m_FaceRecActivity;

        VerIdLoadCallback(final Bundle savedInstanceState, Handler message_handler)
        {
            m_SavedInstanceState = savedInstanceState;
            m_MessageHandler = message_handler;
        }

        @Override
        public void onLoad()
        {
            if (!isDestroyed())
            {
                AsyncTask.execute(new Runnable()
                {
                    @Override
                    public void run()
                    {
                        if (m_SavedInstanceState != null)
                        {
                            verIDSessionResult = m_SavedInstanceState.getParcelable("verIDSessionResult");
                        }

                        Message msg = new Message();
                        Bundle b = new Bundle();

                        // Load captured ID card result from shared preferences
                        idDocument = CardCaptureResultPersistence.loadCapturedDocument(FaceRecActivity.this);

                        // Check that the card has a face that's suitable for recognition
                        if (idDocument != null /*&& idDocument.getFaceSuitableForRecognition() == null*/)
                        {
                            //^^ idDocument = null;
                            // Delete the card, it cannot be used for face recognition
                            //^^ CardCaptureResultPersistence.saveCapturedDocument(MainActivity.this, null);

                            b.putString("id_document_load", "Loaded");

                        }
                        else
                        {
                            b.putString("id_document_load", "NOT Loaded");
                        }

                        msg.setData(b);
                        m_MessageHandler.sendMessage(msg);
                    }
                });
            }
        }

        @Override
        public void onError(Exception var1)
        {
            // loadingIndicatorView.setVisibility(View.GONE);
            Toast.makeText(FaceRecActivity.this, R.string.verid_failed_to_load, Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onCreate(final Bundle savedInstanceState)
    {
        super.onCreate(savedInstanceState);

        // setContentView(R.layout.activity_face_rec);
        // Toolbar toolbar = (Toolbar) findViewById(R.id.toolbar);
        // setSupportActionBar(toolbar);
        // Load Ver-ID
        VerID.shared.load(this, new VerIdLoadCallback(savedInstanceState, new VerIdMessageHandler(this)) );
    }

    @Override
    protected void onDestroy() {
        super.onDestroy();
        // Cleanup
        VerID.shared.unload();
    }

    @Override
    protected void onSaveInstanceState(Bundle outState) {
        super.onSaveInstanceState(outState);
        if (verIDSessionResult != null) {
            outState.putParcelable("verIDSessionResult", verIDSessionResult);
        }
    }

    /*
    protected void deleteIdCaptureResult() {
        if (idDocument != null) {
            VerIDIDCapture.shared.load(this);
            VerIDIDCapture.shared.deleteIDDocument(idDocument);
            idDocument = null;
            CardCaptureResultPersistence.saveCapturedDocument(this, null);
        }
    }


    protected void compareLiveFace() {
        // Ensure we have a valid ID capture result to compare the selfie to
        if (verIDSessionResult != null && verIDSessionResult.isPositive() && !verIDSessionResult.getFaceImages(VerID.Bearing.STRAIGHT).isEmpty() && idDocument != null && idDocument.getFaceSuitableForRecognition() != null) {
            Intent intent = new Intent(this, CaptureResultActivity.class);
            intent.putExtra(CaptureResultActivity.EXTRA_LIVENESS_DETECTION_RESULT, verIDSessionResult);
            startActivity(intent);
        }
    }
    */
}
