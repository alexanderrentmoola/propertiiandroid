package com.rentmoola.app;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

public class RMDataSourceResult extends JSONObject
{
    public RMDataSourceResult(String name)
    {
        try
        {
            put("name", name);
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public void SetStatus(String status)
    {
        try
        {
            put("status", status);
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public void AddAppendedField(String name, String status)
    {
        try
        {
            put(name, status);
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
    }

    public void AddDataSourceField(String name, String status)
    {
        try
        {
            put(name, status);
        } catch (JSONException e)
        {
            e.printStackTrace();
        }
    }


}